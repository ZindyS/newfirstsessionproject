package com.example.onboardingcontroller

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class OnBoardingActivity : AppCompatActivity() {
    val controller = BoardingController()
    lateinit var textView : TextView
    lateinit var imageView : ImageView
    lateinit var button : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
        val list = listOf(Pair("Без теории, только практика\n" +
                "Вы не платите за лекции и теоретический материал, ведь все это можно найти в интернете бесплатно", R.drawable.first),
            Pair("Без теории, только практика\n" +
                    "Вы не платите за лекции и теоретический материал, ведь все это можно найти в интернете бесплатно", R.drawable.second),
            Pair("Обучение онлайн из любой точки мира\n" +
                    "Наше обучение изначально создавалось как дистанционное", R.drawable.img))
        for (i in list) {
            controller.addQueue(i)
        }
        textView = findViewById(R.id.textView)
        imageView = findViewById(R.id.imageView)
        button = findViewById(R.id.button)

        val pair = controller.getCur()
        textView.text = pair.first
        imageView.setImageResource(pair.second)
    }

    fun onButtonClicked(v: View) {
        val res = controller.nextPair()
        if (res == -1) {
            startActivity(Intent(this, SignInActivity::class.java))
            finish()
        } else if (res == 1) {
            button.text = "Начать"
        }
        val pair = controller.getCur()
        textView.text = pair.first
        imageView.setImageResource(pair.second)
    }
}