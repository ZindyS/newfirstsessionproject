package com.example.onboardingcontroller

import org.junit.Test

import org.junit.Assert.*

class ExampleUnitTest {
    //Проверка на то, что элементы очереди вытаскиваются в порядке их добавления
    @Test
    fun buildCheck() {
        val controller = BoardingController()
        val list = listOf(Pair("ABCD", R.drawable.first),
            Pair("BBAB", R.drawable.second),
            Pair("1321", R.drawable.img))
        for (i in list) {
            controller.addQueue(i)
        }


        for (i in list.indices) {
            assertEquals(controller.getCur(), list[i])
            controller.nextPair()
        }
    }

    //Проверка размера
    @Test
    fun sizeCheck() {
        val controller = BoardingController()
        val list = listOf(Pair("ABCD", R.drawable.first),
            Pair("BBAB", R.drawable.second),
            Pair("1321", R.drawable.img))
        for (i in list) {
            controller.addQueue(i)
        }

        val sz = controller.getSize()
        controller.nextPair()
        assertEquals(controller.getSize(),sz-1)
    }
}