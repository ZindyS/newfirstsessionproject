package com.example.onboardingmodule


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class OnBoardingActivityTest {

    @Rule
    @JvmField
    var mActivityScenarioRule = ActivityScenarioRule(OnBoardingActivity::class.java)

    @Test
    fun onBoardingActivityTest() {
        val materialButton = onView(
            allOf(
                withId(R.id.button),
                isDisplayed()
            )
        )
        materialButton.check(matches(withText("Далее")))
        materialButton.perform(click())
        materialButton.perform(click())
        materialButton.check(matches(withText("Начать")))
        materialButton.perform(click())
        onView(withId(R.id.back)).check(matches(isDisplayed()))
    }
}