package com.example.onboardingmodule

import java.util.LinkedList
import java.util.Queue

class BoardingController {
    private val queue : Queue<Pair<String, Int>> = LinkedList()

    fun addQueue (el : Pair<String, Int>) {
        queue.add(el)
    }

    fun getSize() : Int {
        return queue.size
    }

    //0 - кол-во элементов больше 2, 1 - один элемент остался, -1 - элементов нет
    fun nextPair(): Int {
        queue.remove()
        return if (getSize()==0) {
            -1
        } else if (getSize()==1) {
            1
        } else {
            0
        }
    }

    fun getCur() : Pair<String, Int> {
        return queue.element()
    }
}